# Computer Store

Vanilla JS project simulating a computer store.

The bank and pay functionalities, simulate the salary/balance and loans a user can get. The following constraints are in place:

-   User cannot get a loan more than double the bank balance
-   User cannot get more than one bank loan before buying a computer
-   Once a user has a loan, it must be paid it back before getting another loan
-   When transfering money, if there's an outstanding loan, 10% of the salary is deducted towards the outstanding loan

## Deployment

To host the project in Heroku, `index.php` and `composer.json` were added.
Link: https://computer-store-experis.herokuapp.com/

NB: Heroku's dyno sleeps after 30 mins of inactivity, so the first visit can take longer to open the page.
