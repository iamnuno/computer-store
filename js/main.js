import Account from "./account.js";
import laptops from "./laptops.js";

// initial setup
const account = Account();
const elementBalanceText = document.getElementById("balanceText");
const elementPayText = document.getElementById("payText");
const elementLaptopSelect = document.getElementById("laptopSelect");

(() => {
    elementBalanceText.innerText = account.showBalance();
    elementPayText.innerText = account.showPay();
    // show possible laptop selections
    laptops.forEach((laptop) => {
        const option = document.createElement("option");
        const optionText = document.createTextNode(laptop.name);
        option.appendChild(optionText);
        option.value = laptop.name;
        elementLaptopSelect.appendChild(option);
    });
    const laptopOption = elementLaptopSelect.value;
    const laptop = laptops.find((laptop) => laptop.name == laptopOption);
    showLaptop(laptop);
})();

// update displayed laptop
function showLaptop(laptop) {
    const laptopFeatures = laptop.features;
    laptopFeatures.forEach((feature) => {
        const li = document.createElement("li");
        const text = document.createTextNode(feature);
        li.appendChild(text);
        document.getElementById("laptopFeatures").appendChild(li);
    });
    document.getElementById("laptopImg").src = laptop.image;
    document.getElementById("laptopDescription").innerText = laptop.description;
    document.getElementById("laptopPrice").innerText = `${laptop.price} Kr`;
}

// event listeners

// work button
const workButton = document.getElementById("workButton");
workButton.addEventListener("click", () => {
    account.addPay();
    elementPayText.innerText = account.showPay();
});

//bank button
const bankButton = document.getElementById("bankButton");
bankButton.addEventListener("click", () => {
    if (account.getLoan() > 0) {
        account.transferPay(account.getPay());
        document.getElementById("loanText").innerText = account.showLoan();
        // if loan was paid completely
        if (account.getLoan() == 0) {
            document.getElementById("loanText").remove();
            document.getElementById("payLoanButton").remove();
            document.getElementById("getLoanButton").disabled = false;
        }
    } else {
        account.transferPay(account.getPay());
    }
    elementBalanceText.innerText = account.showBalance();
    elementPayText.innerText = account.showPay();
});

// modal save loan button
const saveLoanButton = document.getElementById("saveLoanButton");
saveLoanButton.addEventListener("click", () => {
    const amount = parseInt(document.getElementById("loanAmount").value);
    const elementModalLoanConfirmationText = document.getElementById(
        "modalLoanConfirmationText"
    );
    if (account.grantLoan(amount)) {
        elementModalLoanConfirmationText.innerText = "Loan granted!";
        // new element showing loan
        const loan = document.createElement("p");
        const loanText = document.createTextNode(account.showLoan());
        loan.id = "loanText";
        loan.appendChild(loanText);
        document.getElementById("bank").appendChild(loan);
        // disable get loan button
        getLoanButton.disabled = true;
        // new pay loan button
        const payLoanButton = document.createElement("button");
        payLoanButton.id = "payLoanButton";
        payLoanButton.innerHTML = "Pay Loan";
        payLoanButton.className = "btn btn-primary";
        document.getElementById("workButtonGroup").appendChild(payLoanButton);
        // add event listener for new pay loan button
        payLoanButton.addEventListener("click", () => {
            const excess = account.payLoan(account.getPay());
            // if there's excess get it back
            if (excess > 0) {
                account.setPay(excess);
            } else {
                account.setPay(0);
            }
            elementPayText.innerText = account.showPay();
            // if loan in paid remove loan related info
            if (account.getLoan() == 0) {
                document.getElementById("loanText").remove();
                document.getElementById("payLoanButton").remove();
                // enable button but if the user tries to get a new loan before having
                // a laptop purchase count > 0 it will not be granted
                document.getElementById("getLoanButton").disabled = false;
            } else {
                document.getElementById(
                    "loanText"
                ).innerText = account.showLoan();
            }
        });
    } else {
        elementModalLoanConfirmationText.innerText = "Loan not granted!";
    }
});

// laptop select
const laptopSelect = document.getElementById("laptopSelect");
laptopSelect.addEventListener("change", () => {
    const laptopOption = elementLaptopSelect.value;
    const laptop = laptops.find((laptop) => laptop.name == laptopOption);
    // remove previous laptop's features
    const parent = document.getElementById("laptopFeatures");
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
    // show new laptop selection
    showLaptop(laptop);
});

// buy now button
const buyNowButton = document.getElementById("buyNowButton");
buyNowButton.addEventListener("click", () => {
    const laptopOption = document.getElementById("laptopSelect").value;
    const laptop = laptops.find((laptop) => laptop.name == laptopOption);
    const price = laptop.price;
    // update bank balance and show confirmation text accordingly
    const modalPurchaseConfirmationText = document.getElementById(
        "modalPurchaseConfirmationText"
    );
    if (account.purchaseLaptop(price)) {
        elementBalanceText.innerText = account.showBalance();
        modalPurchaseConfirmationText.innerText = `You have successfully purchased ${laptop.name}!`;
    } else {
        modalPurchaseConfirmationText.innerText =
            "Sorry you don't have enough funds!";
    }
});
