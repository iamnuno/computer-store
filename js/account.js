// Account object holds balance, loan and pay values.
// The exposed functions have the needed functionality to manipulate them.
function Account() {
    let balance = 0;
    let loan = 0;
    let pay = 0;
    let loanCount = 0;
    let laptopPurchaseCount = 0;
    // transfer pay to balance
    // if there's a loan 10% goes towards it
    function transferPay(value) {
        if (loan > 0) {
            const excess = payLoan(value * 0.1);
            balance += value * 0.9 + excess;
        } else {
            balance += value;
        }
        pay = 0;
    }
    // given an amount deduct loan,
    // if there's excess return it
    function payLoan(amount) {
        loan -= amount;
        let excess = 0;
        if (loan < 0) {
            excess = loan *= -1;
            loan = 0;
        }
        return excess;
    }
    // fixed amount to raise pay
    function addPay() {
        pay += 1000;
    }

    function setPay(value) {
        pay = value;
    }
    // given an amount grant loan,
    // return true if grant was conceeded given the requirements
    function grantLoan(amount) {
        if (
            amount > 0 &&
            amount <= balance * 2 &&
            loan == 0 &&
            !(loanCount > 0 && laptopPurchaseCount == 0)
        ) {
            loan = amount;
            loanCount++;
            return true;
        }
        return false;
    }
    // when buying a laptop, update balance
    // return true if there was enough balance for the purchase
    function purchaseLaptop(price) {
        if (price <= balance) {
            balance -= price;
            laptopPurchaseCount++;
            return true;
        }
        return false;
    }

    function getPay() {
        return pay;
    }

    function setBalance(value) {
        balance = value;
    }

    function getBalance() {
        return balance;
    }

    function getLoan() {
        return loan;
    }

    function showBalance() {
        return `Balance: ${balance} Kr`;
    }

    function showPay() {
        return `Pay: ${pay} Kr`;
    }

    function showLoan() {
        return `Loan: ${loan} Kr`;
    }

    return {
        transferPay,
        payLoan,
        addPay,
        grantLoan,
        setPay,
        getPay,
        setBalance,
        getBalance,
        getLoan,
        purchaseLaptop,
        showBalance,
        showPay,
        showLoan,
    };
}

export default Account;
