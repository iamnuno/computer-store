// Mock data
const laptops = [
    {
        name: "ASUS K571 - 15.6",
        price: 1000,
        description:
            "The VivoBook K571 packs a lot punch with it's i7-10750H CPU and 16GB of 2666 RAM. The IPS display is impressive running at a 120 refresh rate.",
        features: ["GeForce GTX 1650 Ti", "16 GB DDR4", "256 GB PCIe SSD"],
        image: "img/laptop1.jpg",
    },
    {
        name: "GIGABYTE AORUS 15G - 15.6",
        price: 2000,
        description:
            "The use of carbon fiber instead of aluminum, magnesium, or plastic is a rare but not unprecedented thing for ultraportable laptops. It's the namesake, after all, of Lenovo's award-winning ThinkPad X1 Carbon, and part of the popular Dell XPS 13.",
        features: ["Intel Core i7-10870H", "1 TB PCIe SSD", "32 GB Memory"],
        image: "img/laptop2.jpg",
    },
    {
        name: "MSI GF65 Thin 9SEXR-838",
        price: 3000,
        description:
            "Four configurations are available, all with a quad-core, 3.3GHz (5.0GHz turbo) Core i7-11375H processor with Intel Iris Xe integrated graphics and a 3,840-by-2,160-pixel non-touch screen. (Core i5 and 1080p models are coming later.)",
        features: ["GeForce RTX 2060", "512 GB SSD", "8 GB DDR4"],
        image: "img/laptop3.jpg",
    },
    {
        name: "ASUS ROG Zephyrus S15",
        price: 4000,
        description:
            "The backlit keyboard has a snappy typing feel and a pleasant layout, though the cursor arrows and top-row Escape and Delete keys are small; you must pair the arrows and Fn key in the absence of dedicated Home, End, Page Up, and Page Down keys. The good-sized touchpad has two slim buttons; it slides and clicks smoothly.",
        features: [
            "32GB RAM",
            "54K/UHD 60 Hz OLED",
            " NVIDIA GeForce RTX 3060",
        ],
        image: "img/laptop4.jpg",
    },
];

export default laptops;
